package ru.penzgtu;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MainTest {

    @Test
    void sqrt1() {
        assertEquals(4, Main.sqrt(25));
    }
    @Test
    void sqrt2() {
        assertNotEquals(10, Main.sqrt(150));
    }

}